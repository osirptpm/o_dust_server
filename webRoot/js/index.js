import setSocketCallback from "/js/socket.js";
import {Queue, Labels, OGraph, OMultiGraph} from "/js/libs/OMultiGraph.js";

let socket;
const devices = {};
const callbacks = {
    connect: (s) => {
        socket = s;
        console.log(s);
    },
    disconnect: (s) => { document.querySelector("article").innerHTML = "" },
    devices: (data) => {
        const {deviceNames, deviceName, type, line} = data;
        // console.log("devices", data);
        const onList = (deviceNames) => {
            console.log("device list:", deviceNames);
            for (const deviceName of deviceNames) {
                devices[deviceName] = initOMultiGraph(deviceName, line);
            }
        };
        const onDelete = (deviceName) => {
            console.log("Delete device", deviceName);
            deleteOMultiGraph(deviceName);
            delete devices[deviceName];
        };
        const onAdd = (deviceName) => {
            if (devices[deviceName]) {
                console.log("Already exist. Delete and Add device", deviceName);
                onDelete(deviceName);
            } else {
                console.log("Add device", deviceName);
            }
            devices[deviceName] = initOMultiGraph(deviceName, line);
        };

        switch (type) {
            case "add": onAdd(deviceName); onresize(null, deviceName); break;
            case "delete": onDelete(deviceName); break;
            default: onList(deviceNames); onresize(null, deviceName); break;
        }
    },
    liveData: (res) => {
        const {deviceName, data} = res;
        console.log("liveData", res);
        // const queue = oGraph.queue;
        // const queue = devices[deviceName].queue;
        // queue.add(pm2_5);
        // for (const graphName of window.devices[device].graphNames) {
        //     const queue = window.devices[device].graphs[graphName].graph.queue;
        //     queue.add(data[graphName]);
        // }

        addQueue(deviceName, data);
        setLabel(deviceName);
        bubbleDispatchEvent(deviceName, data);
        setStatus(deviceName, data);

        // fun
        let isAlert = false;
        for (const el of document.querySelectorAll(".status")) {
            if (el.textContent === "나쁨" || el.textContent === "매우나쁨") {
                isAlert = true;
                break;
            }
        }
        if (isAlert) {
            document.body.classList.add("alert");
        } else {
            document.body.classList.remove("alert");
        }
    },
    responseData: (res) => {
        const {deviceName, data, residualLine} = res;
        console.log("responseData", res);
        addQueue(deviceName, data);
        setLabel(deviceName);
        bubbleDispatchEvent(deviceName, data[data.length - 1]);
        setStatus(deviceName, data[data.length - 1]);
    }
};
setSocketCallback(callbacks);
window.onresize = onresize;

function initOMultiGraph(deviceName, line) {
    const wrap = document.createElement("section");
    wrap.id = deviceName;

    const header = document.createElement("header");

    const h1 = document.createElement("h1");
    h1.textContent = `${deviceName}의 미세먼지`;

    const div = document.createElement("div");
    const span = document.createElement("span");
    span.className = "status";
    span.textContent = `수준`;
    div.append(h1, span);
    header.append(h1, div);

    wrap.append(header);

    const graphNBubble = document.createElement("section");
    graphNBubble.className = "graphNBubble";

    const graph = document.createElement("section");
    graphNBubble.append(graph);

    const bubble = document.createElement("section");
    bubble.append(createBubble(deviceName, "pm2_5"));
    bubble.append(createBubble(deviceName, "pm10"));
    graphNBubble.append(bubble);

    wrap.append(graphNBubble);

    const oGraph_pm2_5 = new OGraph("pm2_5", "rgba(0, 166, 237, .8)", 1, new Labels(), new Queue(line));
    oGraph_pm2_5.dynamicNormalize = false;
    const oGraph_pm10 = new OGraph("pm10", "rgba(127, 184, 0, .8)", 1, new Labels(), new Queue(line));
    oGraph_pm10.dynamicNormalize = false;

    const oMultiGraph = new OMultiGraph(graph, [oGraph_pm2_5, oGraph_pm10]);
    // const labels = new Labels();
    // labels.setY(10, 80, 3, "㎍/㎥");
    // labels.setX(-16, 0, 4, "분");
    // oMultiGraph.integratedLabel(labels);

    document.querySelector("article").append(wrap);
    return oMultiGraph;
}
function deleteOMultiGraph(deviceName) {
    document.getElementById(deviceName).remove();
}
function createBubble(deviceName, pmType) {
    const bubble = document.createElement("section");
    bubble.id = `${deviceName}-${pmType}`;
    bubble.className = `bubble ${pmType}`;
    bubble.style.position = "absolute";
    bubble.style.bottom = "0";
    bubble.style.left = "0";
    bubble.style.display = "none";
    bubble.addEventListener("queue", event => {
        const {deviceName, value} = event.detail;
        const oMultiGraph = devices[deviceName];
        const graph = oMultiGraph.graphs[oMultiGraph.graphNames[0]].graph;
        const min = graph.labels.min.y;
        const max = graph.labels.max.y;
        const height = graph.canvas.height;
        const bottom = (value - min) / (max - min) * height;
        bubble.style.bottom = `${bottom < height? bottom : height}px`;
        bubble.textContent = `${value}㎍/㎥`;
        bubble.style.display = "block";
    });
    return bubble;
}
function bubbleDispatchEvent(deviceName, data) {
    for (const pm of Object.keys(data)) {
        if (data.hasOwnProperty(pm)) {
            if (pm !== "time") {
                const bubble = document.getElementById(`${deviceName}-${pm}`);
                bubble.dispatchEvent(new CustomEvent("queue", {detail: {deviceName, value: data[pm]}}));
            }
        }
    }
}
function setStatus(deviceName, data) {
    const statuses = document.querySelectorAll(`#${deviceName} .status`);
    for (const status of statuses) {
        const {className, textContent} = convertToString(data);
        status.className = className;
        status.textContent = textContent;
    }
}
function convertToString(data) {
    const classNames = ["good", "normal", "warn", "alert"];
    const textContent = ["좋음", "보통", "나쁨", "매우나쁨"];
    const pmVal = {pm2_5: [16, 36, 76], pm10: [31, 81, 151]};
    const indexes = [];
    for (const pm of Object.keys(data).filter(v => v !== "time")) {
        if (data.hasOwnProperty(pm)) {
            indexes.push(pcsToIndex(data[pm], pmVal[pm]));
        }
    }
    const index = Math.max(...indexes);
    return {className: `status ${classNames[index]}`, textContent: textContent[index]};
}
function pcsToIndex(num, arr) {
    if (num < arr[0]) {
        return 0;
    } else if (num < arr[1]) {
        return 1;
    } else if (num < arr[2]) {
        return 2;
    } else {
        return 3;
    }
}


function addQueue(deviceName, data) {
    const strToFloatNTrim = (str) => {
        if (typeof str === "string") return parseFloat(str.replace(/^[\0\s\uFEFF\xA0]+|[\0\s\uFEFF\xA0]+$/g, ''));
        return str;
    };
    const oMultiGraph = devices[deviceName];
    for (const graphName of oMultiGraph.graphNames) {  // ["pm2_5", "pm10"]
        const graph = oMultiGraph.graphs[graphName].graph;
        const queue = graph.queue;

        let pm;
        if (data.constructor.name === "Array") {
            pm = data.map(value => strToFloatNTrim(value[graphName])); // [{pm2_5: 17.9, pm10: 18.8, time: 1557575847698}] => [pm2_5] or [pm10]
        } else { // {pm2_5: 17.9, pm10: 18.8, time: 1557575847698}
            pm = data[graphName];
        }
        queue.add(pm);
    }
}
function setLabel(deviceName) {
    const oMultiGraph = devices[deviceName];
    let min; let max;
    // if (liveData) {
    //     const values = Object.keys(liveData).filter(value => value !== "time").map(value => parseFloat(liveData[value]));
    //     min = Math.min(parseInt(oMultiGraph.graphs[oMultiGraph.graphNames[0]].graph.canvas.dataset.min), ...values);
    //     max = Math.max(parseInt(oMultiGraph.graphs[oMultiGraph.graphNames[0]].graph.canvas.dataset.max), ...values);
    // } else {
        const result = getMinMax(deviceName);
        min = result.min;
        max = result.max;
    // }
    const graph = oMultiGraph.graphs[oMultiGraph.graphNames[0]].graph;
    const queue = oMultiGraph.graphs[oMultiGraph.graphNames[0]].graph.queue;
    const labels = new Labels();
    const paddingValue = 2;
    labels.setY(min - paddingValue, max + paddingValue, parseInt((graph.canvas.height / 100).toString()), "㎍/㎥");
    labels.setX(-(queue.length / 60), 0, 4, "분");

    graph.visibleLabel = true;
    graph.canvas.dataset.min = min;
    graph.canvas.dataset.max = max;

    for (const graphName of oMultiGraph.graphNames) {  // ["pm2_5", "pm10"]
        oMultiGraph.graphs[graphName].graph.labels = labels;
    }
}
function getMinMax(deviceName) {
    const oMultiGraph = devices[deviceName];
    let min = 9999; let max = -9999;
    for (const graphName of oMultiGraph.graphNames) {  // ["pm2_5", "pm10"]
        const queue = oMultiGraph.graphs[graphName].graph.queue;
        min = Math.min(...queue, min);
        max = Math.max(...queue, max);
        oMultiGraph.graphs[graphName].graph.visibleLabel = false;
    }
    return {min, max};
}

function onresize(event, deviceName) {
    console.log(window.innerWidth);
    console.log(document.body.offsetWidth);
    console.log(document.querySelector("h1").offsetWidth);
    const inWidth = document.querySelector("h1").offsetWidth;
    const width = inWidth > 800 ? 800 - 100 - 32 : inWidth - 100 - 32;
    if (deviceName) {
        const graphNames = devices[deviceName].graphNames;
        for (const graphName of graphNames) {
            const canvas = devices[deviceName].graphs[graphName].graph.canvas;
            // const width = inWidth < standardWidth ? inWidth - 100 : standardWidth; // - bubble_width
            const height = width * .6;
            canvas.width = width;
            canvas.height = height;

            canvas.parentElement.style.width = `${width}px`;
            canvas.parentElement.style.height = `${height}px`;

            console.log(devices[deviceName].graphs[graphName].graph);
        }
    } else {
        for (const device in devices) {
            if (devices.hasOwnProperty(device)) {
                const graphNames = devices[device].graphNames;
                for (const graphName of graphNames) {
                    const canvas = devices[device].graphs[graphName].graph.canvas;
                    // const width = inWidth < standardWidth ? inWidth - 100 : standardWidth; // - bubble_width
                    const height = width * .6;
                    canvas.width = width;
                    canvas.height = height;

                    canvas.parentElement.style.width = `${width}px`;
                    canvas.parentElement.style.height = `${height}px`;

                    console.log(devices[device].graphs[graphName].graph);
                }
            }
        }
    }
}
