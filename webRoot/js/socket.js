export default (callbacks) => {
    const socket = io("/dust", {
        path: "/dust-sensor",
        query: {type: "webClient", name: "web-client"},
        transports: ['websocket']
    });

// console.log(`소켓 서버 연결 시도: ${config.url}/${namespace}`);

    socket.on("connect", () => {
        console.log("소켓 서버 접속에 성공했습니다.");
        callbacks.connect(socket);
    });

    socket.on("connect_error", (error) => {
        console.warn("접속에 실패했습니다.", error.message);
    });
    socket.on("connect_timeout", (timeout) => {
        console.warn("연결 시간이 초과되었습니다.");
    });
    socket.on("reconnect", (attemptNumber) => {
        console.log(`재접속에 성공하였습니다.`);
    });
    socket.on("reconnecting", (attemptNumber) => {
        console.warn(`${attemptNumber}번째 재접속 시도 중... `);
    });
    socket.on("error", (error) => {
        console.warn("서버로부터 에러가 발생했습니다.", error);
        socket.close();
        // socket.open();
        setTimeout(() => {socket.open()}, 1000);
    });
    socket.on("disconnect", (reason) => {
        console.warn("서버와 접속이 끊어졌습니다.", reason);
        callbacks.disconnect();
    });
    socket.on("devices", data => {
        // console.log(data);
        callbacks.devices(data);
    });
    socket.on("live-data", data => {
        // console.log(data);
        callbacks.liveData(data);
    });
    socket.on("response-data", data => {
        // console.log(data);
        callbacks.responseData(data);
    });
};