const http = require("http");
const express = require("express");

const socketioInit = require("./socketioInit");

const app = express();
app.use("/", express.static(__dirname + "/webRoot"));
module.exports = app;

const server = http.createServer(app);
socketioInit(server);
server.listen(3000);
