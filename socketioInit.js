const sio = require("socket.io");
const config = require("./config");

const deviceNames = new Set();
const devices = {};
const DEVICE_ROOM = "oFamily-device";
const WEBCLIENT_ROOM = "oFamily-webClient";
const LINE = 1000;

function initClient(socket, namespace, io) {
    const {name, type} = socket.handshake.query;
joinRoom(socket, `oFamily-${type}`);
}
function joinRoom(socket, roomName) {
    const {name, type} = socket.handshake.query;
    socket.join(roomName, () => {
        let rooms = Object.keys(socket.rooms); // [ <socket.id>, 'room 237' ]
        console.log(`${name} joined the ${rooms[1]} room.`);
        switch (type) {
            case "device":
                addDeviceEvent(socket);
                deviceNames.add(name);
                devices[name] = socket;
                socket.to(WEBCLIENT_ROOM).emit("devices", {deviceName: name, type: "add", line: LINE});
                socket.emit("request-data", {line: LINE});
                break;
            case "webClient":
                addWebClientEvent(socket);
                socket.emit("devices", {deviceNames: Array.from(deviceNames), type: "list", line: LINE});
                socket.to(DEVICE_ROOM).emit("request-data", {line: LINE});
                break;
        }
    });
}

function defaultEvent(socket) {
    socket.on("error", (error) => {
        console.log(`Error in ${socket.handshake.query.name ? socket.handshake.query.name : socket.id}.`)
    });
    socket.on("disconnect", (reason) => {
        console.log(`${socket.handshake.query.name ? socket.handshake.query.name : socket.id} is disconnected.`);
    });
    socket.on('disconnecting', (reason) => {
        console.log(`${socket.handshake.query.name ? socket.handshake.query.name : socket.id} is disconnecting.`);
    });
}
function addDeviceEvent(socket) {
    const {name, type} = socket.handshake.query;
    // socket.on("live-data", (data: {pm2_5: number, pm10: number, time: number}) => {
    socket.on("live-data", (data) => {
        console.log(name, "send live-data to the web-clients");
        socket.to(WEBCLIENT_ROOM).emit("live-data", {deviceName: name, data});
    });
    // socket.on("response-data", (data: {data: any[], residualLine: number}) => {
    socket.on("response-data", (data) => {
        console.log(name, "send response-data to the web-clients");
        if (!data) {
            setTimeout(() => {
                socket.emit("request-data", {line: LINE}, 5000);
            });
            return;
        }
        socket.to(WEBCLIENT_ROOM).emit("response-data", {
            deviceName: name,
            data: data.data,
            residualLine: data.residualLine,
        });
    });

    const onDisconnect =  (reason) => {
        deviceNames.delete(name);
        delete devices[name];
        socket.to(WEBCLIENT_ROOM).emit("devices", {deviceName: name, type: "delete"});
    };
    // socket.on("disconnect", onDisconnect);
    socket.on('disconnecting', onDisconnect);
}

function addWebClientEvent(socket) {
    // socket.on("live-data", (data: {pm2_5: number, pm10: number, time: number}) => {
    //     console.log(socket.handshake.query.name, data);
    // });
    // socket.on("request-data", (data: {deviceName: string, line: number}) => {
    socket.on("request-data", (data) => {
        const {deviceName, line} = data;
        socket.emit("devices", {deviceName, type: "add", line});
        devices[deviceName].emit("request-data", {line});
    });
}


function socketioInit(server) {
    const io = sio(server, {
        path: "/dust-sensor",
        transports: ["websocket"],
        serveClient: false
    });

    const dust = io.of("/dust");
    dust.use((socket, next) => {
        defaultEvent(socket);
        if ((socket.handshake.query.key === config.socket_secret_key && socket.handshake.query.name && socket.handshake.query.type) || socket.handshake.query.type && socket.handshake.query.name) {
            return next();
        }
        next(new Error('Authentication error'));
    });
    dust.on("connect", (socket) => {
        console.log(`${socket.handshake.query.name ? socket.handshake.query.name : socket.id} connected.`);
        initClient(socket);
    });
}

module.exports = socketioInit;